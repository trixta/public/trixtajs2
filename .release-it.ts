import type { Config } from 'release-it';

export default {
    git: {
        commit: false,
        tag: true,
        push: true,
        getLatestTagFromAllRefs: true,
        changelog: 'auto-changelog --stdout --commit-limit false -u',
        commitMessage: 'chore: release ${version}',
        pushRepo: 'git@gitlab.com:trixta/public/trixtajs2.git',
        requireCleanWorkingDir: false,
    },
    plugins: {
        '@release-it/conventional-changelog': {
            path: '.',
            infile: 'CHANGELOG.md',
            preset: 'conventionalcommits',
            gitRawCommitsOpts: {
                path: '.',
            },
            strictSemVer: true,
        },
        '@release-it-plugins/workspaces': {
            skipChecks: true,
            ignoreVersion: true,
            publish: true,
            dependencyUpdates: ['./packages/react/package.json'],
        },
        // '@release-it/bumper': {
        //     out: {
        //         file: 'package.json',
        //     },
        // },
    },
    gitlab: {
        release: false,
    },
    github: {
        release: false,
    },
    npm: {
        allowSameVersion: true,
        versionArgs: ['--worspaces false'],
        ignoreVersion: true,
        skipChecks: true,
        publish: false,
    },
} satisfies Config;
