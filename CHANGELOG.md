

## [0.0.2-beta.0](https://gitlab.com/trixta/public/trixtajs2/compare/0.0.1-beta.0...0.0.2-beta.0) (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## 0.1.0-beta.0 (2024-11-21)

### Features

* update readme file, use feat to bump the version ([78b0ca4](https://gitlab.com/trixta/public/trixtajs2/commit/78b0ca4357e9b3db3982f5ce8e07b6b199917f67))

## 0.0.1-beta.0 (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## 0.0.1-beta.0 (2024-11-21)

## [0.1.4-beta.0](https://gitlab.com/trixta/public/trixtajs2/compare/0.1.3-beta.0...0.1.4-beta.0) (2024-11-12)

## [0.1.3-beta.0](https://gitlab.com/trixta/public/trixtajs2/compare/0.1.2-beta.0...0.1.3-beta.0) (2024-11-12)

## [0.1.2-beta.0](https://gitlab.com/trixta/public/trixtajs2/compare/0.1.1-beta.0...0.1.2-beta.0) (2024-11-12)

## [0.1.1-beta.0](https://gitlab.com/trixta/public/trixtajs2/compare/0.1.0-beta.0...0.1.1-beta.0) (2024-11-12)

## 0.1.0-beta.0 (2024-11-12)

### Features

* add vitest to project start testing some data structure changes ([b5952fc](https://gitlab.com/trixta/public/trixtajs2/commit/b5952fc0599c48019f17efed2364cb78c5761215))
* hardening action to stop if there are any issues ([9c206c7](https://gitlab.com/trixta/public/trixtajs2/commit/9c206c7ab14111f8f198f19e6bface4ae6d19c31))
* if join timeout, try again ([0497be2](https://gitlab.com/trixta/public/trixtajs2/commit/0497be262fd4d155c00f5992e3b9f72be15f3263))
* make debug optional ([15fca15](https://gitlab.com/trixta/public/trixtajs2/commit/15fca150c8ea85a920c027b721c72096f4ce14be))
* make the time waited from 3s to 300ms ([f34854b](https://gitlab.com/trixta/public/trixtajs2/commit/f34854b8f19cb328713dff7976c2fe362971ec33))
* migrate the application to typescript, store old reactions ([0fe6204](https://gitlab.com/trixta/public/trixtajs2/commit/0fe6204535f226835d319194b2cc7d3a0d15eca6))
* offline support ([8f11cfa](https://gitlab.com/trixta/public/trixtajs2/commit/8f11cfa7e0568004b4ba0c722151dc0fbf2b286a))
* some fixes ([bef6852](https://gitlab.com/trixta/public/trixtajs2/commit/bef6852a0d9e167eb1504738905217f51fff44b6))
* some fixes ([3c74246](https://gitlab.com/trixta/public/trixtajs2/commit/3c74246ea8673b62ec6acaa34f4eacbc47705a09))
* some updates for react native ([056a693](https://gitlab.com/trixta/public/trixtajs2/commit/056a693444c1e18b2cd17359d17c00fe43909765))
* update actions and reactions automatically ([adca199](https://gitlab.com/trixta/public/trixtajs2/commit/adca19900122a9b147fd3c4aa735c6da2d3f4da8))
* update actions when someone deletes an action ([8e1bcf9](https://gitlab.com/trixta/public/trixtajs2/commit/8e1bcf97efa5b29cb39c344ab75e00b26dac6ea5))
* update interactions view if reactions change ([628df08](https://gitlab.com/trixta/public/trixtajs2/commit/628df08400c7708fc8202330f71fd539f70cfa5e))
* update the package and timing ([55b58f3](https://gitlab.com/trixta/public/trixtajs2/commit/55b58f3817a3fa074795f77f7ea088db5b154d6a))
* validate token, fix an async loading issue ([0a244e8](https://gitlab.com/trixta/public/trixtajs2/commit/0a244e8d3dc471cbfaec3687ea60d94be83192c6))

### Bug Fixes

* a bug fix for actions ([3bc14a0](https://gitlab.com/trixta/public/trixtajs2/commit/3bc14a01b8124176e31b502e23e88bbdcd361b4c))
* add a virtual action check ([d451607](https://gitlab.com/trixta/public/trixtajs2/commit/d451607118b352142c423bf3ca6707cfbd6c83d0))
* add missing file for PR ([de7b990](https://gitlab.com/trixta/public/trixtajs2/commit/de7b990bb6297458a1977b99813ae0669bf6c7ed))
* add the virtual action check on useAction hook ([10d3882](https://gitlab.com/trixta/public/trixtajs2/commit/10d38827d5f2fc6c1c3967ae715c8250e08e837e))
* allow the fetch token to be async ([52f96e8](https://gitlab.com/trixta/public/trixtajs2/commit/52f96e8c1e4f7b096c201829b9ea1aa3b6c8abd2))
* bug fixes or megagig ([7f65ee3](https://gitlab.com/trixta/public/trixtajs2/commit/7f65ee3a8ee5ac2ad964ec9c5f10c30051d5a271))
* bump version ([8994058](https://gitlab.com/trixta/public/trixtajs2/commit/8994058bec813de635dc2ee2393ce751a60b2ffc))
* do not try to reconnect to a channel ([593c36b](https://gitlab.com/trixta/public/trixtajs2/commit/593c36b76dbfcd8ed26fe9090b8241b40c9246cf))
* fix ci file ([ebd659b](https://gitlab.com/trixta/public/trixtajs2/commit/ebd659b726a3d886e8e26f59bb95f649e5dad910))
* fix the type information ([39f7067](https://gitlab.com/trixta/public/trixtajs2/commit/39f70670fc3d7360c75fa80c61c3690c1302f161))
* fix typing info ([9fcf053](https://gitlab.com/trixta/public/trixtajs2/commit/9fcf053340cc1d5330cde378d12398a0fa66c2cc))
* fix typing information ([5ba79ec](https://gitlab.com/trixta/public/trixtajs2/commit/5ba79ecba0864af143264e94e088d45fac6587dd))
* make action nullable for now ([2d8676e](https://gitlab.com/trixta/public/trixtajs2/commit/2d8676e9ee21a17a11ca6afe8bc2e53afaaa272b))
* updated version to 0.0.19 to bring packages in sync ([4461c67](https://gitlab.com/trixta/public/trixtajs2/commit/4461c679bc8a87561d0d46b6f533e412aa2c4941))
* updated version to 0.0.20 to make action() params clearer ([dd4d933](https://gitlab.com/trixta/public/trixtajs2/commit/dd4d933be470c45648b34adc0b2c036118a76128))
* updated version to 0.0.21 to make action() params clearer ([c3e7916](https://gitlab.com/trixta/public/trixtajs2/commit/c3e7916df974d7da0428df916a27d7bb3daed09c))
