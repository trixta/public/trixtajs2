import type { Trixta } from '@trixtajs2/core';
import { useEffect } from 'react';
import { DEFAULT_KEY, useTrixta } from './useTrixta.js';

/**
 * React hook for executing Trixta actions with automatic role management.
 *
 * This hook provides a function to execute actions on a specific role while handling
 * the role's lifecycle (join/leave) automatically. It will:
 * - Join the specified role when the component mounts
 * - Leave the role when the component unmounts
 * - Provide a function to execute the action with optional payload
 *
 * @template T - Type of the action payload
 * @template R - Type of the action response
 *
 * @param options - Configuration options for the action
 * @param {string} options.role - The role name to execute the action under
 * @param {string} options.action - The name of the action to execute
 * @param {string} [options.spaceName=DEFAULT_KEY] - Optional Trixta space name
 * @param {boolean} [options.virtualAction=false] - Whether this is a virtual action
 *
 * @returns {(payload?: T) => Promise<R>} An async function that executes the action
 *   with an optional payload and returns a promise with the response
 *
 * @example
 * ```tsx
 * const submitForm = useAction<FormData, Response>({
 *   role: 'admin',
 *   action: 'submit_form'
 * });
 *
 * // Later in your code:
 * await submitForm({ data: 'example' });
 * ```
 */
export function useAction<T, R>({
    role,
    action,
    spaceName = DEFAULT_KEY,
    virtualAction = false,
}: {
    role: string;
    action: string;
    spaceName?: string;
    virtualAction?: boolean;
}): (payload?: T) => Promise<R> {
    const trixta: Trixta = useTrixta(spaceName);

    // biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
    useEffect(() => {
        if (role) {
            trixta.joinRole(role);

            return () => {
                trixta.leaveRole(role);
            };
        }

        return () => {};
    }, [role]);

    return async (payload: T | undefined = undefined): Promise<R> => {
        return trixta.action<T, R>({
            role,
            actionName: action,
            payload,
            virtualAction,
        });
    };
}
