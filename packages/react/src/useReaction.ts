import type { ReactionSubscription, ReactionType } from '@trixtajs2/core';
import { useEffect, useMemo, useState } from 'react';
import { DEFAULT_KEY, useTrixta } from './useTrixta.js';

type reactionCallback<T> = (v: ReactionType<T>) => Promise<void>;
type SignalFunction<T> = (v: T) => void;

function createChannel<T>() {
    const _callbacks: { [key: number]: SignalFunction<T> } = {};
    let count = 1; // we start at one because 0 is falsy

    function notify(v: T) {
        for (const c of Object.values(_callbacks)) {
            c(v);
        }
    }

    return {
        rx(v: T) {
            notify(v);
        },
        tx(callback: SignalFunction<T>) {
            _callbacks[count] = callback;
            return count++;
        },
        remove(handle: number) {
            delete _callbacks[handle];
        },
    };
}

/**
 * A React hook that subscribes to Trixta reactions and handles their lifecycle.
 *
 * @template T - The type of data expected from the reaction
 *
 * @param {Object} params - The configuration object for the reaction
 * @param {string} [params.spaceName=DEFAULT_KEY] - The name of the Trixta space to connect to
 * @param {string} params.role - The role name to subscribe to
 * @param {string} [params.reaction] - The specific reaction to subscribe to
 *
 * @param {(v: ReactionType<T>) => Promise<void>} [callback] - Callback function that will be called when new reactions are received
 * @param {(v: ReactionType<T>) => Promise<void>} [expired] - Optional callback function that will be called when a reaction expires
 *
 * @returns {ReactionType<T> | undefined} The latest reaction received, or undefined if none or expired
 *
 * @example
 * ```typescript
 * const MyComponent = () => {
 *   const latestReaction = useReaction(
 *     { role: 'myRole', reaction: 'myReaction' },
 *     async (reaction) => {
 *       console.log('New reaction:', reaction);
 *     },
 *     async (expiredReaction) => {
 *       console.log('Reaction expired:', expiredReaction);
 *     }
 *   );
 *
 *   return <div>{JSON.stringify(latestReaction)}</div>;
 * };
 * ```
 */
export function useReaction<T>(
    {
        spaceName = DEFAULT_KEY,
        role: roleName,
        reaction,
    }: { spaceName?: string; role: string; reaction?: string },
    callback: reactionCallback<T> | undefined,
    expired: reactionCallback<T> | undefined = undefined
): ReactionType<T> | undefined {
    const trixta = useTrixta(spaceName);
    const [latestReaction, setLatestReaction] = useState<ReactionType<T>>();

    const callbackChannel = useMemo(() => {
        return createChannel<ReactionType<T>>();
    }, []);

    let reactionSubscription: ReactionSubscription;

    // biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
    useEffect(() => {
        trixta.joinRole(roleName);
        return () => {
            setLatestReaction(undefined);
            trixta.leaveRole(roleName);
        };
    }, [trixta]);

    // biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
    useEffect(() => {
        const handle = callbackChannel.tx((v) => {
            if (v && callback) {
                callback(v);
            }
        });

        return () => {
            callbackChannel.remove(handle);
        };
    }, [callback]);

    // biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
    useEffect(() => {
        let unsubscribe: () => void;

        (async () => {
            reactionSubscription = await trixta.reaction({
                role: roleName,
                reaction,
            });

            if (reactionSubscription.hasPrevious) {
                await Promise.all<unknown>(
                    (reactionSubscription.previous ?? []).map(
                        async (r: ReactionType<T>) => {
                            // the expired messages should have been removed by trixtajs2 core

                            callbackChannel.rx(r);
                        }
                    )
                );
            }

            unsubscribe = reactionSubscription.subscription.subscribe(
                async (r: ReactionType<T>) => {
                    if (r.expired) {
                        setLatestReaction(undefined);
                        if (expired) {
                            await expired(r);
                        }
                    } else {
                        callbackChannel.rx(r);
                        setLatestReaction(r);
                    }
                }
            );
        })();

        return () => {
            if (unsubscribe) {
                unsubscribe();
            }
        };
    }, []);

    return latestReaction;
}
