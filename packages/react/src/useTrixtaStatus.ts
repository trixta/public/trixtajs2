import type { Trixta } from '@trixtajs2/core';
import { useSyncExternalStore } from 'react';
import { DEFAULT_KEY, useTrixta } from './useTrixta.js';

export default function useTrixtaStatus(connectionkey = DEFAULT_KEY) {
    const trixta: Trixta = useTrixta(connectionkey);
    const socketState = trixta.socketState();

    const connectionState = useSyncExternalStore(
        (callback) => {
            const handle = socketState.on(callback);
            return () => {
                socketState.remove(handle);
            };
        },
        () => {
            return socketState.value;
        }
    );

    return connectionState;
}
