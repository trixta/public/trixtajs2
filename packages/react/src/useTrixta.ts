import type { Trixta } from '@trixtajs2/core';
import { createContext, useContext } from 'react';

export const TrixtaContext = createContext<Record<string, Trixta>>({});

export const DEFAULT_KEY: string = 'trixtaDefaultSpace' as const;

/**
 * A React hook that provides access to a Trixta connection instance.
 *
 * @param {string} [connectionKey=DEFAULT_KEY] - The key to identify the Trixta connection.
 * Defaults to 'trixtaDefaultSpace' if not provided.
 *
 * @returns {Trixta} The Trixta connection instance associated with the provided key
 *
 * @throws {Error} If no Trixta connection is found for the provided key
 *
 * @example
 * ```typescript
 * const MyComponent = () => {
 *   const trixta = useTrixta(); // Uses default connection
 *   // or
 *   const customTrixta = useTrixta('customSpace');
 *
 *   return <div>Connected to Trixta</div>;
 * };
 * ```
 */
export function useTrixta(connectionKey: string = DEFAULT_KEY): Trixta {
    const trixtaConnections = useContext(TrixtaContext);

    const trixtaConnection = trixtaConnections[connectionKey];

    if (trixtaConnection === undefined) {
        throw new Error(
            `invalid Trixta connection key "${connectionKey}", valid keys: ${JSON.stringify(
                Object.keys(trixtaConnections)
            )}`
        );
    }

    return trixtaConnection;
}
