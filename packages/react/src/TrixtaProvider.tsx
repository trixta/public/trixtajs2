import {
    type Logger,
    type ReactionStore,
    type Trixta,
    trixta,
} from '@trixtajs2/core';
import { type ReactNode, useContext, useEffect, useState } from 'react';
import { Outlet } from 'react-router';
import { DEFAULT_KEY, TrixtaContext } from './useTrixta.js';

export function TrixtaProvider({
    children = undefined,
    space,
    secure,
    reactionStorage,
    logger = undefined,
    requireAuth,
    tokenFn,
    clearAuth,
    connectionKey = DEFAULT_KEY,
}: {
    children?: ReactNode | ((trixta: Trixta) => ReactNode) | undefined;
    reactionStorage?: ReactionStore;
    space: string;
    secure: boolean;
    logger?: Logger | undefined;
    requireAuth?: () => Promise<void>;
    tokenFn: () => Promise<string | null>;
    clearAuth: () => void;
    connectionKey?: string;
}) {
    const oldTrixta = useContext(TrixtaContext);
    const [t, setT] = useState<Trixta>();

    // biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
    useEffect(() => {
        trixta({
            logger,
            requireAuth: requireAuth ?? (async () => {}),
            reactionStorage:
                reactionStorage ??
                ({
                    get: async () => {
                        return [];
                    },
                    set: async () => {},
                    has: async () => {
                        return false;
                    },
                } satisfies ReactionStore),
            space,
            secure,
            tokenFn,
            clearAuth,
            debug: undefined,
            socketTimeout: undefined,
            pushTimeout: undefined,
        }).then(setT);
    }, []);

    if (oldTrixta[connectionKey] !== undefined) {
        logger?.warn(
            `invalid Trixta connection key "${connectionKey}", this key already exists`
        );
    }

    const newSpaceObject: Record<string, Trixta | undefined> = {};

    if (t === undefined) {
        logger?.debug('returning null component');
        return null;
    }

    newSpaceObject[connectionKey] = t;

    const providerValue = Object.assign(oldTrixta, newSpaceObject);

    logger?.debug('finished setting up new trixta connection');

    if (children) {
        if (typeof children === 'function') {
            logger?.debug('returning render function');
            return (
                <TrixtaContext.Provider value={providerValue}>
                    {children(t)}
                </TrixtaContext.Provider>
            );
        }

        logger?.debug('returning component');
        return (
            <TrixtaContext.Provider value={providerValue}>
                {children}
            </TrixtaContext.Provider>
        );
    }

    logger?.debug('returning router component');
    return (
        <TrixtaContext.Provider value={providerValue}>
            <Outlet />
        </TrixtaContext.Provider>
    );
}
