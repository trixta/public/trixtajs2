import { describe, expect, it } from 'vitest';
import { createObservable, filter } from './observable';

describe('The obserable works', () => {
    it('we can create an observable and see output', async () => {
        let unsubbed = false;

        const ob = createObservable<string>((obs) => {
            obs.next('test');
            obs.complete();

            return () => {
                unsubbed = true;
            };
        });

        const unsubscribe = ob.subscribe((v: unknown) => {
            expect(v).toEqual('test');
        });

        unsubscribe();

        await expect.poll(() => unsubbed === true).toBeTruthy();
    });

    it('we can create an observable and filter out values and see output', async () => {
        let unsubbed = false;

        const ob = createObservable<string>((obs) => {
            obs.next('test 123');
            obs.next('test');
            obs.complete();

            return () => {
                unsubbed = true;
            };
        });

        const unsubscribe = ob
            .pipe(filter((v) => v !== 'test 123'))
            .subscribe((v: unknown) => {
                expect(v).toEqual('test');
            });

        unsubscribe();

        await expect.poll(() => unsubbed === true).toBeTruthy();
    });
});
