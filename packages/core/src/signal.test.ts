import { describe, expect, it } from 'vitest';
import createSignal from './signal';

describe('Test that signals work', () => {
    it('We can create a signal', async () => {
        const signal = createSignal();

        signal.value = 'test';

        expect(signal.value).toEqual('test');
    });

    it('We can create a signal, receive updates and unsubscribe', async () => {
        const signal = createSignal();

        signal.value = 'test';

        const handle = signal.on((v) => {
            expect(v).toEqual('test');
        });

        signal.remove(handle);
    });
});
