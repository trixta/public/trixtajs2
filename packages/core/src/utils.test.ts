import { describe, expect, it } from 'vitest';
import type { ReactionType } from './types';
import { loadNewReactionWithRef } from './utils';

describe('We can successfully manage a list of reactions', () => {
    it('add expired reactions', async () => {
        const reply = async () => {
            return;
        };

        const reactionList = [
            {
                name: 'my_reaction',
                ref: '567',
                role: 'my_role',
                status: undefined,
                settings: {},
                initial_data: {},
                expired: false,
                reply,
            },
            {
                name: 'my_reaction',
                ref: '1234',
                role: 'my_role',
                status: undefined,
                settings: {},
                initial_data: {},
                expired: false,
                reply,
            },
        ];

        const reaction: ReactionType<unknown> = {
            name: 'my_reaction',
            ref: '1234',
            role: 'my_role',
            status: 'expired',
            settings: {},
            initial_data: {},
            expired: true,
            reply,
        };

        const results = loadNewReactionWithRef(reactionList, reaction);

        expect(results).toEqual([
            {
                name: 'my_reaction',
                ref: '567',
                role: 'my_role',
                status: undefined,
                settings: {},
                initial_data: {},
                expired: false,
                reply,
            },
        ]);
    });
    it('add new reactions', async () => {
        const reactionList = [];

        const reply = async () => {
            return;
        };

        const reaction: ReactionType<unknown> = {
            name: 'my_reaction',
            ref: '1234',
            role: 'my_role',
            status: undefined,
            settings: {},
            initial_data: {},
            expired: false,
            reply,
        };

        const results = loadNewReactionWithRef(reactionList, reaction);

        expect(results).toEqual([
            {
                name: 'my_reaction',
                ref: '1234',
                role: 'my_role',
                status: undefined,
                settings: {},
                initial_data: {},
                expired: false,
                reply,
            },
        ]);
    });
});
