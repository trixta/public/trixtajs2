import { Socket as PhoenixSocket } from 'phoenix';
import type { Logger } from './trixta/types.js';

export class Socket {
    _socket: import('phoenix').Socket | undefined;

    initialize(
        domain: string,
        params: Partial<{ token: string | null }>,
        connectionCallbacks: {
            onOpen: () => void;
            onClose: (event: unknown) => void;
            onError: (error: unknown) => void;
        },
        options:
            | Partial<{
                  logger: Logger | undefined;
                  timeout: number;
                  debug: boolean;
                  heartbeatIntervalMs: number;
              }>
            | undefined = undefined
    ) {
        if (!domain) return undefined;
        if (this._socket) this.disconnect(() => {});

        const opt = { params, ...options };

        this._socket = new PhoenixSocket(domain, {
            ...opt,
            debug: opt.debug,
            logger: (kind: string, message: string, data: unknown) => {
                opt.logger?.log(kind, message, data);
            },
        });

        this._socket.onOpen(connectionCallbacks.onOpen);
        this._socket.onClose(connectionCallbacks.onClose);
        this._socket.onError(connectionCallbacks.onError);

        return this._socket;
    }

    connect() {
        this._socket?.connect();
    }

    disconnect(callback: () => void | Promise<void>) {
        this._socket?.disconnect(callback);
        this._socket = undefined;
    }

    connectionState() {
        return this._socket?.connectionState();
    }

    channel(
        topic: string,
        chanParams: object | undefined = undefined
    ): import('phoenix').Channel | undefined {
        return this._socket?.channel(topic, chanParams);
    }

    leaveChannel(topic: string) {
        const channel = this.channel(topic);
        return channel?.leave();
    }
}
