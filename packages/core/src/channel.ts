export type SignalFunction<T> = (v: T) => void;

export function createChannel<T>() {
    const _callbacks: { [key: number]: SignalFunction<T> } = {};
    let count = 1; // we start at one because 0 is falsy

    function notify(v: T) {
        for (const c of Object.values(_callbacks)) {
            c(v);
        }
    }

    return {
        rx(v: T) {
            notify(v);
        },
        tx(callback: SignalFunction<T>) {
            _callbacks[count] = callback;
            return count++;
        },
        remove(handle: number) {
            delete _callbacks[handle];
        },
    };
}
