type RegistrationFunction<T> = (observer: Observer<T>) => () => void;
type OperatorFunction<A, B> = (observableA: Observable<A>) => Observable<B>;
export type Subscription = () => void;
export type Subscriber<T> = Partial<Observer<T>> | ((v: T) => void);

export interface Observer<T> {
    next(value: T): void;
    error(value: unknown): void;
    complete(): void;
}

export interface Observable<T> {
    subscribe(callbacks: Subscriber<T>): Subscription;
    pipe<B>(...operations: OperatorFunction<unknown, unknown>[]): Observable<B>;
}

export function filter<A>(
    predicate: (v: A) => boolean
): OperatorFunction<A, A> {
    return (original) => {
        return createObservable((observer: Observer<A>) => {
            const sub = original.subscribe({
                ...observer,
                next: (value: A) => {
                    if (predicate(value)) {
                        observer.next(value);
                    }
                },
            });

            return sub;
        });
    };
}

const NOOP = () => {};

export function createObservable<T>(
    registerFn: RegistrationFunction<T>
): Observable<T> {
    const observable = {
        pipe: <B>(
            ...operations: OperatorFunction<unknown, unknown>[]
        ): Observable<B> => {
            const result = operations.reduce(
                (accum, operation) => operation(accum),
                observable
            );

            return result as Observable<B>;
        },
        subscribe: (callbacks: Subscriber<T>): Subscription => {
            let destructor = undefined;
            if (typeof callbacks === 'function') {
                destructor = registerFn({
                    next: callbacks || NOOP,
                    error: NOOP,
                    complete: NOOP,
                });
            } else {
                destructor = registerFn({
                    next: callbacks.next || NOOP,
                    error: callbacks.error || NOOP,
                    complete: callbacks.complete || NOOP,
                });
            }
            return destructor || NOOP;
        },
    };

    return observable;
}
