import type { Logger, Role } from './types.js';

/**
 * Sends a reply to a specific reaction event through a Phoenix channel.
 *
 * This function handles replying to reactions by:
 * - Validating that the specified role exists
 * - Managing the role's active state during the reply
 * - Sending the reply through the Phoenix channel
 * - Handling success, error, and timeout scenarios
 *
 * @param options - Configuration object containing roles and properties
 * @param {Record<string, Role>} options.roles - Map of available roles and their configurations
 * @param {Object} options.props - Properties for controlling the reply behavior
 * @param {Logger} [options.props.logger] - Optional logger for error reporting
 * @param {number} [options.props.pushTimeout] - Timeout in milliseconds for the reply (defaults to 160000)
 * @param {string} role - The role name to send the reply through
 * @param {string} ref - Reference identifier for the reaction
 * @param {string} event - The event name being replied to
 * @param {Record<string, unknown>} payload - The data to send with the reply
 * @returns {Promise<void>} Promise that resolves when reply is successful, rejects with error message on failure
 * @throws {Promise<string>} Rejects with error message if role is not joined or reply fails
 */
export default async function reply(
    {
        roles,
        props,
    }: {
        roles: { [key: string]: Role };
        props: {
            logger?: Logger | undefined;
            pushTimeout: number | undefined;
        };
    },
    role: string,
    ref: string,
    event: string,
    payload: Record<string, unknown>
): Promise<void> {
    if (!roles[role]) {
        return Promise.reject(
            `cannot reply to reaction ${event}, role ${role} not joined`
        );
    }

    roles[role].active = true;

    new Promise((resolve, reject) => {
        if (roles[role]) {
            roles[role].channel
                .push(
                    `reply:${ref}`,
                    {
                        event,
                        value: payload,
                    },
                    props.pushTimeout || 160000
                )
                .receive('ok', (response: unknown) => {
                    roles[role].active = false;
                    resolve(response);
                })
                .receive('error', (response: unknown) => {
                    roles[role].active = false;
                    props.logger?.error(
                        `error replying to reaction ${role}:${event}`,
                        response
                    );
                    reject(`error replying to reaction ${role}:${event}`);
                })
                .receive('timeout', (response: unknown) => {
                    roles[role].active = false;
                    props.logger?.error(
                        `timeout replying to reaction ${role}:${event}`,
                        response
                    );
                    reject(`timeout replying to reaction ${role}:${event}`);
                });
        }
    });
}
