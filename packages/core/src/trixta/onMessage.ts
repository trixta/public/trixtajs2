import type { SignalFunction } from '../channel.js';
import { mergeDeep } from '../deepMerge.js';
import type {
    InteractionSettings,
    ReactionList,
    ReactionStore,
    ReactionType,
    RoleSettings,
} from '../types.js';
import buildReaction from './buildReaction.js';
import type { Logger, Roles } from './types.js';
import { loadNewReactionWithRef } from './utils.js';

type Props<T> = {
    role: string;
    roles: Roles;
    event: string;
    payload: ReactionType<T>;
    reactionReceivedChannel: {
        rx(v: ReactionType<unknown>): void;
        tx(callback: SignalFunction<ReactionType<unknown>>): number;
        remove(handle: number): void;
    };
    props: {
        reactionStorage: ReactionStore;
        pushTimeout: number | undefined;
        logger?: Logger | undefined;
    };
};

/**
 * Synchronizes Trixta system data by updating role settings based on received payload type.
 *
 * This function handles four types of synchronization:
 * - 'actions': Updates all contract actions for a role
 * - 'reactions': Updates all contract reactions for a role
 * - 'action': Merges a single new action into existing contract actions
 * - 'reaction': Merges a single new reaction into existing contract reactions
 *
 * @param options - Configuration object for sync operation
 * @param {Roles} options.roles - Map of all available roles and their current states
 * @param {string} options.role - The specific role being updated
 * @param {ReactionType<unknown>} options.payload - The payload containing:
 *   - initial_data: {
 *       type: 'actions' | 'reactions' | 'action' | 'reaction';
 *       data: any;
 *     }
 * @returns {void}
 */
function syncTrixtaData({
    roles,
    role,
    payload,
}: {
    roles: Roles;
    role: string;
    payload: ReactionType<unknown>;
}): void {
    // default value
    const initData = payload.initial_data as {
        type: string;
        data: unknown;
    };

    // an update to all actions
    if (initData.type === 'actions') {
        const actions = initData.data;

        const value: RoleSettings =
            roles[role].settings.value ??
            ({
                contract_reactions: {},
                contract_actions: {},
            } satisfies RoleSettings);

        roles[role].settings.value = Object.assign(
            roles[role].settings.value ?? {},
            {
                contract_actions: actions as Record<
                    string,
                    InteractionSettings
                >,
                contract_reactions: value.contract_reactions,
            } satisfies RoleSettings
        );
    }

    // an update to all reactions
    if (initData.type === 'reactions') {
        const data = initData.data;

        const value =
            roles[role].settings.value ??
            ({
                contract_reactions: {},
                contract_actions: {},
            } satisfies RoleSettings);
        roles[role].settings.value = Object.assign(
            roles[role].settings.value ?? {},
            {
                contract_reactions: data as Record<string, InteractionSettings>,
                contract_actions: value.contract_actions,
            } satisfies RoleSettings
        );
    }

    // an update to an action
    if (initData.type === 'action') {
        const newAction = {
            [(initData.data as { name: string }).name]: initData.data,
        };
        roles[role].settings.value = mergeDeep(
            roles[role].settings.value ?? {
                contract_reactions: {},
                contract_actions: {},
            },
            {
                contract_actions: newAction,
            }
        );
    }

    // an update to a reaction
    if (initData.type === 'reaction') {
        const newReaction = {
            [(initData.data as { name: string }).name]: initData.data,
        };

        roles[role].settings.value = mergeDeep(
            roles[role].settings.value ?? {
                contract_reactions: {},
                contract_actions: {},
            },
            {
                contract_reactions: newReaction,
            }
        );
    }
}

/**
 * Handles incoming messages and system synchronization for Trixta roles.
 *
 * This handler manages message processing and role state updates by:
 * - Processing system synchronization messages ('sync_data_trixta_sys')
 * - Updating role settings for actions and reactions
 * - Managing reaction lifecycles and storage
 * - Handling reaction accumulation for tagged reactions
 *
 * @template T
 * @param {Props} options - Configuration object containing:
 * @param {string} options.role - The role name being processed
 * @param {Record<string, Role>} options.roles - Map of available roles and their states
 * @param {string} options.event - The event name received from the channel
 * @param {ReactionType<T>} options.payload - The message payload
 * @param {Object} options.reactionReceivedChannel - Channel for reaction communication
 * @param {Function} options.reactionReceivedChannel.rx - Receiver function for reactions
 * @param {Function} options.reactionReceivedChannel.tx - Transmitter function for reactions
 * @param {Function} options.reactionReceivedChannel.remove - Function to remove handlers
 * @param {Object} options.props - Additional properties
 * @param {ReactionStore} options.props.reactionStorage - Storage for reactions
 * @param {number | undefined} options.props.pushTimeout - Optional timeout for operations
 * @param {Logger | undefined} options.props.logger - Optional logging interface
 * @returns {ReactionType<T>} The processed message payload
 */
export default function onMessage<T>({
    role,
    roles,
    event,
    payload,
    reactionReceivedChannel,
    props,
}: Props<T>): ReactionType<T> {
    const filtederedEvents = [
        'chan_reply',
        'phx_reply',
        'phx_error',
        'phx_close',
    ];

    if (event === 'sync_data_trixta_sys') {
        syncTrixtaData({
            roles,
            role,
            payload,
        });
        return;
    }

    // the event is from the Trixta system create a reaction, if it's control flow event from Phoenix ignore it.
    if (!filtederedEvents.find((i) => event.startsWith(i))) {
        const reaction = buildReaction<T>(roles, props, {
            name: event,
            role,
            payload,
        });

        /**
         * Handles reaction storage based on accumulation tag presence
         *
         * @remarks
         * For reactions tagged with '__trixta_sys_accumulate_reactions':
         * - Retrieves existing {@link ReactionList} from storage using role:name key
         * - Merges new reaction with existing list using {@link loadNewReactionWithRef}
         * - Updates storage with merged list
         *
         * For untagged reactions:
         * - Stores reaction directly as single-item array in {@link ReactionStore}
         *
         * @example
         * Storage key format: `${role}:${event}`
         * Tagged storage: loadNewReactionWithRef(existingList, newReaction)
         * Untagged storage: [reaction]
         */
        if (roles[reaction.role]) {
            if (
                roles[reaction.role].settings.value.contract_reactions[
                    reaction.name
                ]?.tags?.includes('__trixta_sys_accumulate_reactions')
            ) {
                props.reactionStorage
                    .get(`${reaction.role}:${reaction.name}`)
                    .then((listReaction: ReactionList) => {
                        props.reactionStorage.set(
                            `${role}:${event}`,
                            loadNewReactionWithRef(listReaction, reaction)
                        );
                    });
            } else {
                props.reactionStorage.set(`${role}:${event}`, [reaction]);
            }

            props.logger?.debug('sending onreaction to subscribers', reaction);

            // send reaction to subscribers
            reactionReceivedChannel.rx(reaction);
        }
    }

    return payload;
}
