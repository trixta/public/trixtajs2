import { type Observable, filter } from '../observable.js';

import type {
    ReactionStore,
    ReactionSubscription,
    ReactionType,
} from '../types.js';

/**
 * Creates a filtered reaction subscription based on provided role and reaction parameters.
 *
 * This function manages reaction subscriptions by:
 * - Filtering reactions based on role and/or reaction name
 * - Checking for previous reactions in storage
 * - Creating an observable subscription with appropriate filters
 *
 * @param options - Configuration object for the reaction
 * @param {Object} options.props - Properties object containing storage
 * @param {ReactionStore} options.props.reactionStorage - Storage interface for reactions
 * @param {Observable<ReactionType<any>>} options.observable - Observable stream of reactions
 * @param filterOptions - Options to filter the reaction subscription
 * @param {string} [filterOptions.role] - Optional role to filter reactions by
 * @param {string} [filterOptions.reaction] - Optional reaction name to filter by
 * @returns {Promise<ReactionSubscription>} Promise resolving to a reaction subscription object containing:
 *   - hasPrevious: boolean indicating if there's a previous reaction
 *   - previous: previously stored reaction (if any)
 *   - subscription: filtered Observable of reactions
 */
export default async function reaction(
    {
        props,
        observable,
    }: {
        props: {
            reactionStorage: ReactionStore;
        };
        observable: Observable<ReactionType<unknown>>;
    },
    {
        role = undefined,
        reaction = undefined,
    }: {
        role?: string;
        reaction?: string;
    }
): Promise<ReactionSubscription> {
    if (reaction) {
        if (role) {
            return {
                hasPrevious: await props.reactionStorage.has(
                    `${role}:${reaction}`
                ),
                previous: await props.reactionStorage.get(
                    `${role}:${reaction}`
                ),
                subscription: observable.pipe(
                    filter(
                        (r: ReactionType<unknown>) =>
                            r.name === reaction && r.role === role
                    )
                ),
            };
        }
        return {
            hasPrevious: false,
            previous: undefined,
            subscription: observable.pipe(
                filter((r: ReactionType<unknown>) => r.name === reaction)
            ),
        };
    }

    if (role) {
        return {
            hasPrevious: false,
            previous: undefined,
            subscription: observable.pipe(
                filter((r: ReactionType<unknown>) => r.role === role)
            ),
        };
    }

    return {
        hasPrevious: false,
        previous: undefined,
        subscription: observable,
    };
}
