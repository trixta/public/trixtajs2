import type { ReactionList, ReactionType } from '../types.js';

export function validateToken(token: string | null): boolean {
    if (!token) {
        return false;
    }

    try {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        const exp = JSON.parse(window.atob(base64)).exp;

        return new Date() < new Date(exp * 1000);
    } catch {
        return false;
    }
}

export function agentIdFromToken(token: string | null) {
    if (!token) {
        return '';
    }

    try {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64)).agent_id;
    } catch {
        return '';
    }
}

export function loadNewReactionWithRef(
    reactionList: ReactionList,
    reaction: ReactionType<unknown>
) {
    if (reaction.expired === false) {
        return [...reactionList, reaction];
    }
    return reactionList.filter((r) => {
        return r.ref !== reaction.ref;
    });
}
