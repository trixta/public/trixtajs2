import { describe, expect, it } from 'vitest';
import action from './action';
import type { Roles } from './types';

describe('Test that action function works', () => {
    it('Test that we fail when we have a broken connection', async () => {
        const roles: Roles = {};
        const a = action(
            {
                socket: {
                    connectionState: () => 'closed',
                },
                roles,
                props: {},
            },
            {
                role: 'test',
                actionName: 'test',
            }
        );

        await expect(a).rejects.toEqual(
            'connection closed when trying to call test:test'
        );
    });
    it('Test that we fail when we have not loaded role metadata', async () => {
        const roles: Roles = {};
        const a = action(
            {
                socket: {
                    connectionState: () => 'open',
                },
                roles,
                props: {},
            },
            {
                role: 'test',
                actionName: 'test',
            }
        );

        await expect(a).rejects.toEqual(
            'tried calling an action on a non joined role: test:test'
        );
    });
});
