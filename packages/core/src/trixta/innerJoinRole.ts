import type { RoleSettings } from '../types.js';
import type { Logger, Role } from './types.js';

export default async function innerJoinRole(
    {
        roles,
        props,
    }: {
        roles: { [key: string]: Role };
        props: { logger?: Logger | undefined };
    },
    role: string,
    channel: import('phoenix').Channel,
    retry: number
): Promise<RoleSettings> {
    return new Promise<RoleSettings>((resolve, reject) => {
        channel
            .join(10000)
            .receive('ok', (response: RoleSettings) => {
                props.logger?.debug(`sucessfully joined role ${role}`);
                roles[role].settings.value = response;

                resolve(response);
            })
            .receive('error', (e) => {
                props.logger?.error(e);
                reject(`failed to join role: ${role} ${JSON.stringify(e)}`);
            })
            .receive('timeout', () => {
                if (retry > 0) {
                    return innerJoinRole(
                        { roles, props },
                        role,
                        channel,
                        retry - 1
                    );
                }

                setTimeout(() => {
                    reject(`timeout joining role: ${role}`);
                }, 2000);
            });
    });
}
