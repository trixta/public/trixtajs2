import type { ConnectionState } from 'phoenix';
import type { Logger, Role } from './types.js';

/**
 * Executes an action on a specified role through a Phoenix channel.
 *
 * This function handles the communication between the client and server by:
 * - Validating the socket connection is open
 * - Checking if the requested role exists and is accessible
 * - Managing role settings and metadata
 * - Supporting retry logic for when role settings aren't immediately available
 * - Handling virtual actions that may not be defined in the contract
 * - Managing timeouts and error states
 *
 * @param options - Object containing socket, roles and props
 * @param options.socket - The Phoenix socket connection
 * @param options.roles - Map of available roles and their configurations
 * @param options.props - Optional configuration including logger and push timeout
 * @param actionOptions - Object containing action parameters
 * @param actionOptions.role - The role to execute the action on
 * @param actionOptions.actionName - The name of the action to execute
 * @param actionOptions.payload - Optional data to send with the action
 * @param actionOptions.retry - Number of retry attempts for fetching role settings (default: 3)
 * @param actionOptions.virtualAction - Whether the action is virtual and may not exist in contract (default: false)
 * @returns Promise resolving to the action response or rejecting with an error
 */
export default async function action<T, R>(
    {
        socket,
        roles,
        props: { logger = undefined, pushTimeout = undefined },
    }: {
        socket: {
            connectionState: () => ConnectionState | undefined;
        };
        roles: { [key: string]: Role };
        props: Partial<{
            pushTimeout: number | undefined;
            logger: Logger | undefined;
        }>;
    },
    {
        role,
        actionName,
        payload,
        retry = 3,
        virtualAction = false,
    }: {
        role: string;
        actionName: string;
        payload?: T;
        retry?: number;
        virtualAction?: boolean;
    }
): Promise<R> {
    if (socket.connectionState() !== 'open') {
        logger?.error('tried to call action with but socket is not connected', {
            role,
            action: actionName,
        });
        return Promise.reject(
            `connection closed when trying to call ${role}:${actionName}`
        );
    }

    const p = payload || {};

    if (!roles[role]) {
        logger?.error(
            `tried calling an action on a non joined role: ${role}:${actionName}`
        );
        return Promise.reject(
            `tried calling an action on a non joined role: ${role}:${actionName}`
        );
    }
    roles[role].active = true;

    const roleSettings = roles[role].settings.value;

    if (!roleSettings) {
        if (retry > 0) {
            return Promise.any<R>([
                new Promise((res, err) => {
                    const handle = roles[role].settings.on((v) => {
                        if (v) {
                            action(
                                {
                                    props: { logger, pushTimeout },
                                    socket,
                                    roles,
                                },
                                {
                                    role,
                                    actionName,
                                    payload,
                                    retry: retry - 1,
                                    virtualAction,
                                }
                            )
                                .then((r) => {
                                    roles[role].settings.remove(handle);
                                    return r;
                                })
                                .then((r) => res(r as R))
                                .catch((e) => {
                                    roles[role].settings.remove(handle);
                                    logger?.error(e);
                                    return err(e);
                                });
                        } else {
                            logger?.warn('role settings is missing');
                        }
                    });
                }),
                new Promise((_, err) => {
                    logger?.error('Timeout when fetching role metadata');

                    setTimeout(() => {
                        err('Timeout when fetching role metadata');
                    }, 3000);
                }),
            ]);
        }
        logger?.error(
            `timed out waiting for connection when trying to call ${role}:${actionName}`
        );
        return Promise.reject(
            `timed out waiting for connection when trying to call ${role}:${actionName}`
        );
    }

    if (virtualAction === false) {
        if (!roleSettings.contract_actions[actionName]) {
            return Promise.reject(
                `Action ${actionName} does not exit on role ${role}`
            );
        }
    }

    return new Promise<R>((resolve, reject) => {
        if (roles[role]) {
            roles[role].channel
                .push(actionName, p as object, pushTimeout || 60000)
                .receive('ok', (response: unknown) => {
                    roles[role].active = false;
                    resolve(response as R);
                })
                .receive('error', (response: unknown) => {
                    roles[role].active = false;
                    logger?.error(
                        `error calling action ${role}:${actionName}`,
                        response
                    );
                    reject(`error calling action ${role}:${actionName}`);
                })
                .receive('timeout', (response: unknown) => {
                    roles[role].active = false;
                    logger?.error(
                        `timeout calling action ${role}:${actionName}`,
                        response
                    );
                    reject(`timeout calling action ${role}:${actionName}`);
                });
        }
    });
}
