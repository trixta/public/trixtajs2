import type { Logger, Role } from './types.js';

export default async function leaveRole(
    {
        roles,
        props,
    }: {
        roles: { [key: string]: Role };
        props: {
            logger?: Logger | undefined;
        };
    },
    role: string
) {
    let timer: number;

    return new Promise((resolve, reject) => {
        if (roles[role]) {
            if (roles[role].active === false) {
                roles[role].arc -= 1;
                if (roles[role].arc <= 0) {
                    roles[role].channel
                        .leave()
                        .receive('ok', (response: unknown) => {
                            clearInterval(timer);
                            delete roles[role];
                            resolve(response);
                        })
                        .receive('error', (response: unknown) => {
                            clearInterval(timer);
                            props.logger?.error(
                                `error leaving role: ${role}`,
                                response
                            );
                            reject(`error leaving role: ${role}`);
                        })
                        .receive('timeout', (response: unknown) => {
                            clearInterval(timer);
                            props.logger?.error(
                                `timeout leaving role: ${role}`,
                                response
                            );
                            reject(`timeout leaving role: ${role}`);
                        });
                } else {
                    clearInterval(timer);
                    resolve({});
                }
            }
        } else {
            clearInterval(timer);
            resolve({});
        }
    });
}
