import type { SignalFunction } from '../signal.js';
import type { Socket } from '../socket.js';
import type { ReactionStore, ReactionType } from '../types.js';
import { curry } from './curry.js';
import joinRole from './joinRole.js';
import leaveRole from './leaveRole.js';
import type { Logger, Roles } from './types.js';
import { agentIdFromToken } from './utils.js';

export default async function authenticate(
    {
        requireAuth,
        authAgentSignal,
        roles,
        socket,
        props,
        reactionReceivedChannel,
    }: {
        requireAuth: () => Promise<void>;
        authAgentSignal: {
            value: unknown;
            on(callback: SignalFunction<unknown>): number;
            reset(): void;
            remove(handle: number): void;
        };
        roles: Roles;
        socket: Socket;
        reactionReceivedChannel: {
            rx(v: ReactionType<unknown>): void;
            tx(callback: SignalFunction<ReactionType<unknown>>): number;
            remove(handle: number): void;
        };
        props: {
            space: string;
            secure: boolean | undefined;
            logger?: Logger | undefined;
            debug: boolean | undefined;
            pushTimeout: number | undefined;
            socketTimeout: number | undefined;
            reactionStorage: ReactionStore;
        };
    },
    connectionCallbacks: {
        onOpen: () => void;
        onClose: (event: unknown) => void;
        onError: (error: unknown) => void;
    },
    token: string
): Promise<void> {
    return new Promise((resolve, reject) => {
        if (
            socket.connectionState() === 'open' ||
            socket.connectionState() === 'connecting'
        ) {
            const currentRoles = Object.keys(roles);

            socket.disconnect(async () => {
                authAgentSignal.value = agentIdFromToken(token);

                await Promise.all(
                    currentRoles.map(
                        curry(leaveRole)({
                            props,
                            roles,
                        })
                    )
                );

                socket.initialize(
                    `${props.secure ? 'wss' : 'ws'}://${props.space}/socket`,
                    {
                        token,
                    },
                    connectionCallbacks,
                    {
                        timeout: props.socketTimeout || 60000,
                        debug: props.debug,
                        heartbeatIntervalMs: 30000,
                    }
                );

                socket.connect();

                await Promise.all(
                    currentRoles.map(
                        curry(joinRole)({
                            requireAuth,
                            socket,
                            props,
                            roles,
                            reactionReceivedChannel,
                        })
                    )
                );

                resolve();
            });
        } else {
            reject();
        }
    });
}
