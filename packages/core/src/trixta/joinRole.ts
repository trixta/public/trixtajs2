import type { SignalFunction } from '../channel.js';
import createSignal from '../signal.js';
import type { Socket } from '../socket.js';
import type { ReactionStore, ReactionType } from '../types.js';
import innerJoinRole from './innerJoinRole.js';
import onMessage from './onMessage.js';
import type { Logger, Role } from './types.js';

/**
 * Joins a specified role channel and manages its lifecycle in the Trixta system.
 *
 * This function handles the role joining process by:
 * - Managing role reference counting (arc)
 * - Establishing Phoenix channel connections
 * - Setting up message handlers for reactions and actions
 * - Managing role settings and synchronization
 * - Handling authentication requirements
 *
 * @param options - Configuration object for joining the role
 * @param {() => Promise<void>} options.requireAuth - Function to handle authentication requirements
 * @param {Object} options.reactionReceivedChannel - Channel for handling reactions
 * @param {(v: ReactionType<any>) => void} options.reactionReceivedChannel.rx - Reaction receiver function
 * @param {(callback: SignalFunction<ReactionType<any>>) => number} options.reactionReceivedChannel.tx - Reaction transmitter function
 * @param {(handle: number) => void} options.reactionReceivedChannel.remove - Function to remove reaction handlers
 * @param {Object} options.props - Configuration properties
 * @param {ReactionStore} options.props.reactionStorage - Storage for reactions
 * @param {number | undefined} options.props.pushTimeout - Timeout for push operations
 * @param {Logger | undefined} options.props.logger - Optional logger instance
 * @param {Socket} options.socket - Phoenix socket instance
 * @param {Record<string, Role>} options.roles - Map of available roles
 * @param {string} role - The name of the role to join
 * @returns {Promise<void>} Promise that resolves when role is joined successfully
 * @throws {Promise<string>} Rejects with error message if joining fails or authentication is required
 */
export default async function joinRole(
    {
        requireAuth,
        reactionReceivedChannel,
        socket,
        roles,
        props,
    }: {
        requireAuth: () => Promise<void>;
        reactionReceivedChannel: {
            rx(v: ReactionType<unknown>): void;
            tx(callback: SignalFunction<ReactionType<unknown>>): number;
            remove(handle: number): void;
        };
        props: {
            reactionStorage: ReactionStore;
            pushTimeout: number | undefined;
            logger?: Logger | undefined;
        };
        socket: Socket;
        roles: { [key: string]: Role };
    },
    role: string
): Promise<void> {
    if (role === null) {
        return;
    }

    if (roles[role]) {
        roles[role].arc += 1;
        return;
    }

    let channel = socket.channel(`space:${role}`);

    if (channel) {
        roles[role] = {
            arc: 1,
            channel,
            active: false,
            settings: createSignal(undefined),
        };

        props.logger?.debug(`joining role ${role}`);

        return innerJoinRole({ roles, props }, role, channel, 3)
            .then(async () => {
                if (channel) {
                    channel.onMessage = (
                        event: string,
                        payload: ReactionType<unknown>
                    ): ReactionType<unknown> =>
                        onMessage({
                            role,
                            roles,
                            reactionReceivedChannel,
                            props,
                            event,
                            payload,
                        });
                }
            })
            .catch(async (error: string) => {
                props.logger?.error(error);

                if (error.includes('unauthorized')) {
                    await requireAuth();
                }

                if (channel) {
                    channel.leave();
                }
                channel = undefined;

                return Promise.reject(error);
            });
    }

    return Promise.reject('Failed creating channel');
}
