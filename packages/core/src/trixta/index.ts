import { createChannel } from '../channel.js';
import { createObservable } from '../observable.js';
import createSignal from '../signal.js';
import { Socket } from '../socket.js';
import type { ReactionStore, ReactionType } from '../types.js';
import action from './action.js';
import authenticate from './authenticate.js';
import { curry, curry2 } from './curry.js';
import joinRole from './joinRole.js';
import leaveRole from './leaveRole.js';
import reaction from './reaction.js';
import type { Logger, Roles } from './types.js';
import { agentIdFromToken, validateToken } from './utils.js';

type Props = {
    space: string;
    secure: boolean | undefined;
    debug: boolean | undefined;
    socketTimeout: number | undefined;
    pushTimeout: number | undefined;
    tokenFn: () => Promise<string | null>;
    clearAuth: () => void;
    reactionStorage: ReactionStore;
    logger?: Logger | undefined;
    requireAuth: () => Promise<void>;
};

export type Trixta = Awaited<ReturnType<typeof trixta>>;

export type SocketConnectionTypes =
    | {
          state: 'initialize';
      }
    | {
          state: 'open';
      }
    | {
          state: 'close';
          event: unknown;
      }
    | {
          state: 'error';
          error: unknown;
      };

export async function trixta(props: Props) {
    props.logger?.debug('creating trixta client');

    const reactionReceivedChannel = createChannel<ReactionType<unknown>>();
    const socketState = createSignal<SocketConnectionTypes>({
        state: 'initialize',
    });

    const token = await props.tokenFn();

    const state = {
        requireAuth: async () => {
            if (state.requireAuthCalled === false && token === null) {
                state.requireAuthCalled = true;
                await props.requireAuth();
            }
        },
        requireAuthCalled: false,
        authAgentSignal: createSignal(undefined),
        reactionReceivedChannel,
        observable: createObservable<ReactionType<unknown>>((obs) => {
            const handle = reactionReceivedChannel.tx((v) => obs.next(v));

            return () => {
                reactionReceivedChannel.remove(handle);
            };
        }),
        props,
        socket: new Socket(),
        roles: {} as Roles,
    };

    const socketConnectionCallbacks = {
        onError: (error: unknown) => {
            socketState.value = {
                state: 'error',
                error,
            };
        },
        onOpen: () => {
            socketState.value = {
                state: 'open',
            };
        },
        onClose: (event: unknown) => {
            socketState.value = {
                state: 'close',
                event,
            };
        },
    };

    if (validateToken(token)) {
        props.logger?.debug('connecting to trixta with auth');
        state.authAgentSignal.value = agentIdFromToken(token);
        state.socket.initialize(
            `${props.secure ? 'wss' : 'ws'}://${props.space}/socket`,
            {
                token,
            },
            socketConnectionCallbacks,
            {
                timeout: props.socketTimeout || 10000,
                debug: props.debug,
                heartbeatIntervalMs: 30000,
            }
        );
    } else {
        props.logger?.debug('connecting to trixta without auth');
        state.authAgentSignal.value = null;
        state.socket.initialize(
            `${props.secure ? 'wss' : 'ws'}://${props.space}/socket`,
            {},
            socketConnectionCallbacks,
            {
                timeout: props.socketTimeout || 10000,
                debug: props.debug,
                heartbeatIntervalMs: 30000,
            }
        );
    }

    props.logger?.debug('requesting trixta connection');

    try {
        state.socket.connect();
    } catch {
        props.logger?.error('failed making the connection');
        // NOP
    }

    const timers: unknown[] = [];

    await Promise.any([
        new Promise<void>((resolve) => {
            timers.push(
                setInterval(() => {
                    if (state.socket.connectionState() === 'open') {
                        props.logger?.info(
                            'connection to trixta has been established'
                        );
                        timers.forEach(clearInterval);
                        resolve();
                    }
                }, 100)
            );
        }),
        new Promise<void>((resolve) => {
            timers.push(
                setInterval(() => {
                    if (
                        state.socket.connectionState() === 'closed' ||
                        state.socket.connectionState() === 'closing'
                    ) {
                        props.logger?.error(
                            'failed connecting to trixta, error'
                        );
                        timers.forEach(clearInterval);
                        resolve();
                    }
                }, 100)
            );
        }),
        //timeout
        new Promise<void>((resolve) => {
            timers.push(
                setInterval(() => {
                    if (state.socket.connectionState() === 'connecting') {
                        props.logger?.error(
                            'failed connecting to trixta, timeout'
                        );
                        //
                        timers.forEach(clearInterval);
                        resolve();
                    }
                }, 10000)
            );
        }),
    ]);

    props.logger?.debug('returning trixta client');

    return {
        action: <T, R>(args: {
            role: string;
            actionName: string;
            payload?: T;
            retry?: number;
            virtualAction?: boolean;
        }) => action<T, R>(state, args),
        roles: () => {
            return Object.keys(state.roles);
        },
        authAgent: () => {
            return state.authAgentSignal;
        },
        reaction: curry(reaction)(state),
        joinRole: curry(joinRole)(state),
        disconnect: async () => {
            state.authAgentSignal.value = undefined;
            return new Promise<void>((resolve) => {
                props.clearAuth();
                state.socket.disconnect(resolve);
            });
        },
        socketState: () => {
            return socketState;
        },
        authenticate: curry(curry2(authenticate)(state))(
            socketConnectionCallbacks
        ),
        leaveRole: curry(leaveRole)(state),
        isAuth: () => {
            return validateToken(token);
        },
        roleSettings: (role: string) => {
            if (state.roles[role]) {
                return state.roles[role].settings;
            }
            return undefined;
        },
    };
}
