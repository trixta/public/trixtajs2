import type createSignal from '../signal.js';
import type { RoleSettings } from '../types.js';

export type Roles = Record<string, Role>;

export type Role = {
    channel: import('phoenix').Channel;
    active: boolean;
    settings: ReturnType<typeof createSignal<RoleSettings | undefined>>;
    arc: number;
};

export type Logger = {
    log(...data: unknown[]): void;
    debug(...data: unknown[]): void;
    info(...data: unknown[]): void;
    warn(...data: unknown[]): void;
    error(...data: unknown[]): void;
};
