import type { ReactionStore, ReactionType, SettingsType } from '../types.js';
import reply from './reply.js';
import type { Logger, Roles } from './types.js';

export default function buildReaction<T>(
    roles: Roles,
    props: {
        reactionStorage: ReactionStore;
        pushTimeout: number | undefined;
        logger?: Logger | undefined;
    },
    {
        name,
        role,
        payload,
    }: {
        name: string;
        role: string;
        payload: {
            ref: string;
            status: 'expired' | undefined;
            settings: SettingsType;
            initial_data?: T;
            message?: T;
        };
    }
): ReactionType<T> {
    if (payload.message) {
        return {
            role,
            name,
            ref: undefined,
            status: undefined,
            settings: {},
            initial_data: payload.message,
            expired: false,
            reply: Promise.resolve,
        };
    }

    return {
        role,
        name,
        ref: payload.ref,
        status: payload.status,
        settings: payload.settings,
        initial_data: payload.initial_data,
        expired: payload.status === 'expired',
        reply(p: Record<string, unknown>) {
            if (payload.ref) {
                return reply(
                    {
                        roles,
                        props,
                    },
                    role,
                    payload.ref,
                    name,
                    p
                );
            }
            return Promise.resolve();
        },
    };
}
