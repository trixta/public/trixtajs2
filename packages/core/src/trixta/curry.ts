export function curry<A, B, C>(f: (a: A, b: B) => C) {
    return (a: A) => (b: B) => f(a, b) as C;
}

export function curry2<A, B, C, D>(f: (a: A, b: B, c: C) => D) {
    return (a: A) => (b: B, c: C) => f(a, b, c) as D;
}
