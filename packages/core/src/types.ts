export type ReactionType<T> = {
    role: string;
    name: string;
    status: 'expired' | undefined;
    settings: SettingsType;
    ref: string | undefined;
    initial_data: T;
    expired: boolean;
    reply: (p: Record<string, unknown>) => Promise<void>;
};

export type ReactionList = ReactionType<unknown>[];

export type ReactionStore = {
    has: (key: string) => Promise<boolean>;
    get: (key: string) => Promise<ReactionList>;
    set: (key: string, reactions: ReactionList) => Promise<void>;
};

export type InteractionSettings = {
    request_schema: unknown;
    request_settings: unknown;
    tags: string[];
};

export type RoleSettings = {
    contract_reactions?: Record<string, InteractionSettings>;
    contract_actions?: Record<string, InteractionSettings>;
};

export type ReactionSubscription = {
    hasPrevious: boolean;
    previous: ReactionList | undefined;
    subscription: import('./observable.js').Observable<ReactionType<unknown>>;
};

export type SchemaType = {
    properties: Record<
        string,
        { type: 'string'; title?: string; description?: string }
    >;
};

export type SettingsType = {
    'ui:globalOptions'?: unknown;
    'ui:submitButtonOptions'?: {
        submitText?: string;
        norender?: boolean;
        props: React.ButtonHTMLAttributes<HTMLButtonElement>;
    };
    'ui:options'?: Record<string, unknown>;
    'ui:field'?: string;
    'ui:widget'?: string;
};
