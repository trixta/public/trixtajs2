import type { ReactionList, ReactionType } from './types.js';

export function loadNewReactionWithRef(
    reactionList: ReactionList,
    reaction: ReactionType<unknown>
) {
    if (reaction.expired === false) {
        return [...reactionList, reaction];
    }
    return reactionList.filter((r) => {
        return r.ref !== reaction.ref;
    });
}
