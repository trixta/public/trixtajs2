export type SignalFunction<T> = (v: T) => void;

type G<T> = T | null | undefined;

export default function createSignal<T>(v: G<T> = undefined) {
    let _val: T | null;

    if (v !== undefined) {
        _val = v;
    }
    const _callbacks: { [key: number]: SignalFunction<T | null> } = {};
    let count = 1; // we start at one because 0 is falsy

    function notify() {
        for (const c of Object.values(_callbacks)) {
            c(_val);
        }
    }

    return {
        get value(): T | null {
            return _val;
        },
        set value(v: T | null) {
            _val = v;
            notify();
        },
        on(callback: SignalFunction<T | null>) {
            if (_val !== undefined) {
                callback(_val);
            }
            _callbacks[count] = callback;
            return count++;
        },
        reset() {
            for (const k of Object.keys(_callbacks)) {
                delete _callbacks[Number.parseInt(k)];
            }
        },
        remove(handle: number) {
            delete _callbacks[handle];
        },
    };
}
