import type { ReactionList, ReactionStore } from './types.js';

export function createMemoryReactionStore(): ReactionStore {
    const hashMap: Record<string, ReactionList> = {} as Record<
        string,
        ReactionList
    >;

    async function has(key: string): Promise<boolean> {
        return (
            hashMap[key] !== undefined &&
            (hashMap[key] ?? { length: 0 }).length > 0
        );
    }

    return {
        has,
        get: async (key: string): Promise<ReactionList> => {
            if (await has(key)) {
                return hashMap[key] ?? [];
            }
            return [];
        },
        set: async (key: string, reactions: ReactionList): Promise<void> => {
            hashMap[key] = [...reactions];
        },
    };
}
