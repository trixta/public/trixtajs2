export function isObject(item: unknown): boolean {
    return (
        item !== undefined && typeof item === 'object' && !Array.isArray(item)
    );
}

export function mergeDeep(target: unknown, ...sources: unknown[]) {
    if (!sources.length) return target;
    const source = sources.shift();

    if (isObject(target) && isObject(source)) {
        for (const key in source as Record<string, unknown>) {
            if (isObject(source[key])) {
                if (!target[key]) Object.assign(target, { [key]: {} });
                target[key] = mergeDeep(target[key], source[key]);
            } else {
                Object.assign(target, { [key]: source[key] });
            }
        }
    }

    return mergeDeep(target, ...sources);
}
