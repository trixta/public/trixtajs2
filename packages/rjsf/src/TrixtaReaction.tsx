import { Form } from '@rjsf/mui';
import { DEFAULT_KEY, type RJSFSchema, type UiSchema } from '@rjsf/utils';
import validator from '@rjsf/validator-ajv8';
import type {
    ReactionType,
    RoleSettings,
    SchemaType,
    SettingsType,
    Trixta,
} from '@trixtajs2/core';
import { useReaction } from '@trixtajs2/react';
import { type FunctionComponent, useEffect, useRef, useState } from 'react';
import { SubmitContext } from './submitContext.js';

type Props = {
    trixta: Trixta;
    role: string;
    reaction: string;
    join?: boolean;
    data?: unknown;
    showForm: boolean;
    allReactions?: boolean;
    hideData?: boolean;
    hideHeader?: boolean;
    onReaction?: (r: ReactionType<unknown>) => void;
    formFields: Record<string, FunctionComponent<unknown>>;
    afterSubmit?: () => Promise<void>;
    spaceName?: string;
};

export function TrixtaReaction({
    trixta,
    role,
    reaction,
    data = undefined,
    hideHeader = true,
    allReactions = false,
    hideData = true,
    onReaction,
    showForm = true,
    formFields,
    afterSubmit,
    spaceName = DEFAULT_KEY,
}: Props) {
    const [schema, setSchema] = useState<SchemaType>();
    const [settings, setSettings] = useState<SettingsType>();

    const submitDataRef = useRef<unknown>();

    // we need to join the role for the schema information for the reaction
    useEffect(() => {
        trixta.joinRole(role);

        return () => {
            trixta.leaveRole(role);
        };
    }, [trixta, role]);

    // this use effect Gets the schema settigns via a signal
    useEffect(() => {
        if (trixta.roleSettings) {
            const v = trixta.roleSettings(role)?.value;

            if (v?.contract_reactions?.[reaction]) {
                setSchema(
                    v?.contract_reactions[reaction].request_schema as SchemaType
                );
                setSettings(
                    v?.contract_reactions[reaction]
                        .request_settings as SettingsType
                );
            }

            const handle = trixta.roleSettings(role)?.on((v: RoleSettings) => {
                if (v?.contract_reactions?.[reaction]) {
                    setSchema(
                        v?.contract_reactions[reaction]
                            .request_schema as SchemaType
                    );
                    setSettings(
                        v?.contract_reactions[reaction]
                            .request_settings as SettingsType
                    );
                }
            });

            return () => {
                if (handle) {
                    trixta.roleSettings(role)?.remove(handle);
                }
            };
        }

        return () => {};
    }, [trixta, role, reaction]);

    const latestReaction = useReaction(
        {
            role,
            reaction,
            spaceName,
        },
        async (r: ReactionType<unknown>) => {
            if (onReaction) {
                onReaction(r);
            }

            setSettings(r.settings as SettingsType);
        }
    );

    const uiCustomField = latestReaction?.settings?.[
        'ui:field'
    ] as keyof typeof formFields;

    const CustomComponent: FunctionComponent<{
        formData: unknown;
        settings: unknown;
    }> = formFields[uiCustomField];

    const uiSubmitButtonOptions = settings?.['ui:submitButtonOptions'];

    const isCustom =
        typeof uiCustomField === 'string' &&
        Object.hasOwn(formFields, uiCustomField);

    async function onSubmit() {
        const data = submitDataRef?.current ?? latestReaction.initial_data;
        if (data) {
            await latestReaction.reply(data as Record<string, unknown>);
            if (afterSubmit) {
                await afterSubmit();
            }
        }
    }

    const props = {
        ...{
            className:
                'bg-purple-600 hover:bg-purple-400 text-purple-100 font-bold px-3 py-1 rounded-md',
            type: 'button' as const,
        },
        ...uiSubmitButtonOptions?.props,
    };

    return (
        (latestReaction || allReactions) &&
        showForm && (
            <div
                data-role={role}
                data-reaction={reaction}
                className={`${
                    hideHeader
                        ? ''
                        : 'bg-surface1 rounded-lg border-2 border-purple-700 p-4 ring ring-purple-950'
                }`}
            >
                <SubmitContext.Provider
                    value={{
                        submit: () => {
                            onSubmit();
                        },
                    }}
                >
                    {' '}
                    <>
                        {!hideHeader && (
                            <div className="text-white">
                                {role} - {reaction}
                            </div>
                        )}
                        {(latestReaction?.initial_data as object) &&
                            !hideData && (
                                <div className="bg-surface0 mt-4 max-w-96 overflow-y-auto rounded border border-gray-300 p-4 text-gray-200">
                                    {JSON.stringify(
                                        latestReaction?.initial_data ?? {}
                                    )}
                                </div>
                            )}
                        {isCustom && (
                            <CustomComponent
                                formData={latestReaction.initial_data ?? {}}
                                settings={latestReaction.settings ?? {}}
                            />
                        )}
                        {!isCustom && (
                            <Form
                                formData={latestReaction.initial_data ?? data}
                                tagName="div"
                                onChange={(e) => {
                                    if (
                                        e.formData &&
                                        Object.keys(e.formData).length > 0
                                    ) {
                                        submitDataRef.current = e.formData;
                                    }
                                }}
                                schema={schema ?? {}}
                                fields={formFields}
                                uiSchema={
                                    (settings as UiSchema<
                                        unknown,
                                        RJSFSchema,
                                        unknown
                                    >) ?? {}
                                }
                                // @ts-expect-error some fields have the wrong typing info, but they work
                                validator={validator}
                            >
                                {uiSubmitButtonOptions?.norender !== true && (
                                    <div
                                        className={`flex justify-end ${hideHeader ? '' : 'mt-2'}`}
                                    >
                                        <button
                                            data-role={role}
                                            data-reaction={reaction}
                                            onClick={onSubmit}
                                            {...props}
                                        >
                                            {uiSubmitButtonOptions?.submitText ??
                                                'Submit'}
                                        </button>
                                    </div>
                                )}
                            </Form>
                        )}
                    </>
                </SubmitContext.Provider>
            </div>
        )
    );
}
