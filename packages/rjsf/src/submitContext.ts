import { createContext } from 'react';

// Create a context for the submit function
export const SubmitContext = createContext({
    submit: () => {},
});
