import type { IChangeEvent } from '@rjsf/core';
import { Form } from '@rjsf/mui';
import type { RJSFSchema, UiSchema } from '@rjsf/utils';
import validator from '@rjsf/validator-ajv8';
import type {
    RoleSettings,
    SchemaType,
    SettingsType,
    Trixta,
} from '@trixtajs2/core';
import { useAction } from '@trixtajs2/react';
import {
    type FunctionComponent,
    type MutableRefObject,
    useEffect,
    useRef,
    useState,
} from 'react';
import { SubmitContext } from './submitContext.js';

type Props = {
    trixta: Trixta;
    role: string;
    data: unknown;
    action: string;
    join?: boolean;
    hideHeader: boolean;
    spaceName?: string;
    formFields: Record<string, FunctionComponent>;
    onSubmit: (v: unknown) => Promise<void>;
    logger?: Console | undefined;
};

export function TrixtaAction({
    trixta,
    role,
    action,
    data,
    hideHeader,
    formFields,
    spaceName,
}: Props) {
    const [schema, setSchema] = useState<SchemaType>();
    const [settings, setSettings] = useState<SettingsType>();

    const divRef = useRef(null);
    const submitDataRef = useRef<unknown>();

    // we need to join the role for the schema information for the reaction

    // biome-ignore lint/correctness/useExhaustiveDependencies: don't respond to trixta changes
    useEffect(() => {
        trixta.joinRole(role);

        return () => {
            trixta.leaveRole(role);
        };
    }, []);

    // this use effect Gets the schema settigns via a signal
    useEffect(() => {
        if (trixta.roleSettings) {
            if (trixta.roleSettings(role)?.value) {
                const v = trixta.roleSettings(role)?.value;
                if (v?.contract_actions[action]) {
                    setSchema(
                        v?.contract_actions[action].request_schema as SchemaType
                    );
                    setSettings(
                        v?.contract_actions[action]
                            .request_settings as SettingsType
                    );
                }
            }

            const handle = trixta.roleSettings(role)?.on((v: RoleSettings) => {
                if (v.contract_actions[action]) {
                    setSchema({
                        ...(v?.contract_actions[action]
                            .request_schema as SchemaType),
                    });
                    setSettings({
                        ...(v?.contract_actions[action]
                            .request_settings as SettingsType),
                    });
                }
            });

            return () => {
                if (handle) {
                    trixta.roleSettings(role)?.remove(handle);
                }
            };
        }

        return () => {};
    }, [trixta, role, action]);

    const callTrixtaAction = useAction({
        role,
        action,
        spaceName,
    });

    const uiCustomField = settings?.['ui:field'] as keyof typeof formFields;

    const uiSubmitButtonOptions = settings?.['ui:submitButtonOptions'];

    const isCustom = uiCustomField && Object.hasOwn(formFields, uiCustomField);

    if (uiCustomField) {
        // logger.log('rendering custom component', formFields, uiCustomField);
    }

    const CustomComponent: FunctionComponent<{
        schema: SchemaType;
        settings: SettingsType;
        actionCall: (v: unknown) => void;
        submitDataRef: MutableRefObject<unknown>;
    }> = formFields[uiCustomField];

    const props = {
        ...{
            className:
                'bg-green-600 hover:bg-green-400 text-green-100 font-bold px-3 py-1 rounded-md',
            type: 'button' as const,
        },
        ...uiSubmitButtonOptions?.props,
    };

    return (
        <div data-role={role} data-action={action}>
            {schema && (
                <SubmitContext.Provider
                    value={{
                        submit: () => {
                            callTrixtaAction(submitDataRef.current);
                            return false;
                        },
                    }}
                >
                    <div
                        className={`${
                            hideHeader
                                ? ''
                                : 'rounded-lg border-2 border-green-700 p-4 ring ring-green-950'
                        }`}
                    >
                        {!hideHeader && (
                            <div className="text-white">
                                {role} - {action}
                            </div>
                        )}
                        {isCustom && (
                            <CustomComponent
                                schema={
                                    schema ?? {
                                        properties: {},
                                    }
                                }
                                settings={settings}
                                actionCall={callTrixtaAction}
                                submitDataRef={submitDataRef}
                            />
                        )}
                        {!isCustom && (
                            <Form
                                ref={divRef}
                                schema={schema}
                                formData={data}
                                fields={formFields}
                                tagName="div"
                                onChange={(
                                    e: IChangeEvent<unknown, typeof schema>
                                ) => {
                                    if (e.formData) {
                                        submitDataRef.current = e.formData;
                                    }
                                }}
                                uiSchema={
                                    settings as UiSchema<
                                        unknown,
                                        RJSFSchema,
                                        unknown
                                    >
                                }
                                // @ts-expect-error some fields have the wrong typing info, but they work
                                validator={validator}
                            >
                                {uiSubmitButtonOptions?.norender !== true && (
                                    <div
                                        className={`flex justify-end ${hideHeader ? '' : 'mt-2'}`}
                                    >
                                        <button
                                            onClick={async () => {
                                                await callTrixtaAction(
                                                    submitDataRef.current
                                                );
                                            }}
                                            {...props}
                                        >
                                            {uiSubmitButtonOptions?.submitText ??
                                                'Submit'}
                                        </button>
                                    </div>
                                )}
                            </Form>
                        )}
                    </div>
                </SubmitContext.Provider>
            )}
        </div>
    );
}
