import { fixupConfigRules, fixupPluginRules } from '@eslint/compat';
import { FlatCompat } from '@eslint/eslintrc';
import js from '@eslint/js';
import typescriptEslint from '@typescript-eslint/eslint-plugin';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import tsParser from '@typescript-eslint/parser';
import vitest from '@vitest/eslint-plugin';
import globals from 'globals';
import path from 'node:path';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
    baseDirectory: __dirname,
    recommendedConfig: js.configs.recommended,
    allConfig: js.configs.all,
});

export default [
    {
        ignores: ['**/dist', '**/.eslintrc.cjs'],
    },
    ...fixupConfigRules(
        compat.extends(
            'eslint:recommended',
            'plugin:react/recommended',
            'plugin:react/jsx-runtime',
            'plugin:react-hooks/recommended',
            'plugin:prettier/recommended',
            'plugin:@typescript-eslint/recommended'
        )
    ),
    {
        plugins: {
            vitest,
            '@typescript-eslint': fixupPluginRules(typescriptEslint),
        },

        languageOptions: {
            parserOptions: {
                warnOnUnsupportedTypeScriptVersion: false,
            },
            globals: {
                ...globals.browser,
                ...globals.node,
            },

            parser: tsParser,
            ecmaVersion: 'latest',
            sourceType: 'module',
        },
        settings: {
            react: {
                version: '18.2',
            },
        },
        rules: {
            ...vitest.configs.recommended.rules,
            'prettier/prettier': [
                'error',
                {
                    endOfLine: 'auto',
                },
            ],
            'react-hooks/exhaustive-deps': 'off',
            'react/prop-types': 'off',
            '@typescript-eslint/no-explicit-any': 'off',
            '@typescript-eslint/no-unused-vars': [
                'error',
                {
                    vars: 'all',
                    args: 'after-used',
                    ignoreRestSiblings: false,
                },
            ],
            '@typescript-eslint/no-require-imports': 'off',
            'react/jsx-uses-react': 2,
            'react/jsx-uses-vars': 'error',
            'no-console': 'error',
            'no-await-in-loop': 'error',
            'no-const-assign': 'error',
            'object-shorthand': ['error', 'always'],
        },
    },
];
